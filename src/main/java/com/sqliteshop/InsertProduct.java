/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqliteshop;
import Model.Product;
import com.sqliteshop.Database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TUFGaming
 */
public class InsertProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO PRODUCT (name,price) VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Product product = new Product(-1,"Oh Leing",20 );
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            int row = stmt.executeUpdate();
            
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
                
            }
            System.out.println("Affect row "+row+" id "+id);
        } catch (SQLException ex) {
            Logger.getLogger(SelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

       db.close();
    }
}